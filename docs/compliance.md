# Java Compliance Issues

ExtendJ does not currently have perfect compliance with the various versions of
the Java specification. It is a long-term goal to have perfect Java compliance for Java 5+.
ExtendJ does have a version for Java 4, but this version is not actively tested.

ExtendJ does not currently support Java 9, but we are working on it.

### Annotations

ExtendJ does not support annotation processors. Annotation processors work on
the Java AST, but ExtendJ uses its own AST structure and thus can not run
annotation processors.

### Java 8

ExtendJ has not been updated to support all type inference cases that the Java 8
specification allows. The current type inference algorithm is based on the
Java 5 specification, which does not allow nested type inference.

Currently, we have made promising progress on full Java 8 type inference. The
latest beta version is on the typeinf2 branch in the main Bitbucket repository.

### Java 9

The small language changes in Java 9 will be implemented soon in ExtendJ. The
module system requires more work and may take some time to implement.

## Issue Tracker

For a list of all current known issues, see [the issue tracker][1].  Please
report any compliance issues you may encounter! We are always very interested
to know about any differences you may find between javac and ExtendJ.

[1]: https://bitbucket.org/extendj/extendj/issues?status=new&status=open
