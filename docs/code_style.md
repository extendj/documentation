#Code Style

Using a consistent code style improves readability. The coding style used
for ExtendJ is based on [Google's Java Style Guide][1], with a few special
considerations for JastAdd code:

* Synthesized and inherited attributes should not share the same name.
  Synthesized and inherited attributes are fundamentally different and if two
  different kinds of attributes share the same name then it can easily lead to
  unexpected behaviour.
* The `refine` constructs can cause lines to be very long. If a `refine` line
  is longer than 100 characters, then a line break should be inserted
  after the aspect name part of the refinement.
* If an attribute equation is too long to fit on the line, prefer to add a line
  break after the first equals sign rather than inserting a line break in the
  middle of the expression.

[1]: http://google.github.io/styleguide/javaguide.html
