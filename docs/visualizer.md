# AST Exploring Tool

[ExtendJ Explorer][1] is an interactive tool for exploring the ASTs built by
ExtendJ.

The tool shows a graphical view of the parsed AST alongside the Java source
code, and it even lets you inspect attributes for particular nodes in the AST!

The AST can be filtered based on a flexible filter rule language.

Here is a screenshot of the application in use:

![ExtendJ Explorer Screenshot](/drast.png)

The latest version of ExtendJ Explorer can be downloaded here:

* [ExtendJ Explorer from Bitbucket][2]


## Requirements

ExtendJ Explorer requires Java 8 update 40 or later to run.

If you use OpenJDK 8 on Ubuntu, the `openjfx` package is also needed.  This is
because OpenJDK does not bundle the JavaFX libraries which ExtendJ Explorer
uses.  Run the following command in a terminal to install the `openjfx` on
Ubuntu:

    sudo apt-get install openjfx


The JavaFX libraries are not included in OpenJDK 9 for Debian/Ubuntu.  If you
use Java 9 on Linux, you may need to use Oracle JDK rather than OpenJDK.  The
Oracle distributions bundle JavaFX.


## Usage Instructions

The center pane graphs the current AST. Hold Ctrl and click+drag the mouse to
navigate the view.

The right-hand pane has a source editor for the source code corresponding to
the current AST.  Java source code can be loaded or typed directly in the
source editor.  Click the "Rebuild AST" button to refresh the tree view after
editing the source code.

The left-hand pane displays information about the attributes of the current
selected AST node.

## Filtering Nodes

A filter can be entered in the filter editor. The filter language consists of
three different kinds of rules:

* `include` - includes matched nodes in the filtered tree
* `exclude` - filters out the matched nodes, and their subtrees
* `set` - only used to change properties for matched node

Each rule has a path pattern consisting of the type names for nodes on
the path from the tree root, or wildcard expressions.

Each rule can also provide a matching condition based on node attributes,
and add style and attribute info for the matched nodes.

## DrAST - A visualizer for any JastAdd project

ExtendJ Explorer is a fork of [DrAST](https://bitbucket.org/jastadd/drast),
which can be used with any JastAdd compiler - not just ExtendJ.
DrAST was the Master's Thesis project of Joel Lindholm and Johan Thorsberg.


[1]: https://bitbucket.org/extendj/extendj-explorer/overview
[2]: https://bitbucket.org/extendj/extendj-explorer/downloads/ExJExplorer-1.2.2.jar
