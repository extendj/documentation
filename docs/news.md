# News

## 2023-08-15 Version 11.0.0 release

ExtendJ 11.0.0 adds support for Java 9 through 11 thanks to the contributions of
David Björk and Johannes Aronsson.

## 2018-12-12 Version 8.1.2 release

ExtendJ 8.1.2 fixes miscellaneous issues.

Change log for this version:

* Fixed bytecode parsing issue regarding synthetic fields [(issue 297)](https://bitbucket.org/extendj/extendj/issues/297).
* Allow multi-catch parameters to be declared final [(issue 298)](https://bitbucket.org/extendj/extendj/issues/298).
* Removed several redundant error messages for: method lookup, assignment, return type, imports, field initialization [(issue 249)](https://bitbucket.org/extendj/extendj/issues/249).
* Improved protected member access control [(issue 299)](https://bitbucket.org/extendj/extendj/issues/299).
* Removed ID token from ThisAccess and SuperAccess as it was unused.
* Simplified type inference implementation.
* Updated `SimpleSet` implementation: made the interface public and removed the full set.
* Removed TypeVariable rewrite [(issue 301)](https://bitbucket.org/extendj/extendj/issues/301).
* Replaced all uses of `TypeDecl.instanceOf(TypeDecl)` by `TypeDecl.subtype(TypeDecl)`.
* Fixed incorrect local index in for-each [(issue 302)](https://bitbucket.org/extendj/extendj/issues/302).
* Fixed incorrect name lookup in for-each [(issue 303)](https://bitbucket.org/extendj/extendj/issues/303).
* Do not crash if build/version info is missing.
* Ignore non-standard command line options.


## 2018-11-12 String Repetition Example

A new example project has been published, demonstrating how to build a small extension
which adds string repetition functionality for the multiplication expression. For example,
allowing you to write this in a Java program:

    System.out.println("-" * 30);


The repository for this example project is found here:
[https://bitbucket.org/extendj/string-repeat/](https://bitbucket.org/extendj/string-repeat/)

## 2018-04-04 Type Inference Beta

We are working on improving Java 8 type inference. A beta version of the new
type inference is available in the typeinf2 branch from the Bitbucket repository.

## 2018-01-25 Release 8.1.1

ExtendJ 8.1.1 fixes many bytecode generation issues, and a few static analysis bugs.

Only minor API changes were made since version 8.1.0.

Full change log:

  * Unified diamond type inference with method type inference.
    A synthetic method access is now used by `DiamondAccess` to infer type
    arguments. This means that much more of the method type inference
    implementation can be reused directly by the diamond implementation.
  * Removed attributes:
      - `DiamondAccess.mostSpecific(_, _)`
      - `DiamondAccess.potentiallyApplicable(_)`
      - `DiamondAccess.applicableBySubtyping(_, _)`
      - `DiamondAccess.applicableByMethodInvocationConversion(_, _)`
      - `DiamondAccess.applicableByVariableArity(_, _)`
  * Changed the argument type of `MethodAccess.maxSpecific()` from Collection to Iterable.
  * Changed the Argument type of `MethodAccess.potentiallyApplicable()` from
    `Collection` to `Iterable`.
  * Changed `TypeDecl.bridgeMethods()` from collection attribute to synthesized
    attribute.
  * Removed the attribute `TypeDecl.localBridgeMethods()` (renamed to
    "bridgeMethods").
  * The attribute `SwichStmt.enumIndices()` has been moved to `TypeDecl`.
    This attribute is used to generate implicit `enum` switch maps.
  * Elements in the `SimpleSet` datastructure is are now ordered by insertion
    order. This makes the element order deterministic.
  * Added NTA `TryWithResources.closeAccess()` to represent the implicit
    resource closing method access in a TWR statement.
  * Removed `TryWithResources.lookupClose()`.
    Use `TryWithResources.closeAccess().decl()` instead.
  * Added attribute `TypeDecl.methods()`, which computes all visible method
    declarations for a type (includes inherited methods).
  * Removed `TypeDecl.methodsIterator()`.
    Use `TypeDecl.methods().iterator()` instead.
  * Added attribute `TypeDecl.interfacesMethods()`, which computes all methods
    inherited from superinterfaces (transitive).
  * Removed `TypeDecl.interfacesMethodsIterator()`.
    Use `TypeDecl.interfacesMethods().iterator()` instead.
  * Added attribute `TypeDecl.localMethods()`, which computes all local method
    declarations for a type.
  * Removed `TypeDecl.localMethodsIterator()`.
    Use `TypeDecl.localMethods().iterator()` instead.
  * Removed `TypeDecl.interfacesIterator()`.
    Use `TypeDecl.superInterfaces().iterator()` instead.

## 2017-12-30 Release 8.1.0

ExtendJ is now stable enough that it makes sense to tag a release.  Version
8.1.0 includes several large rewrites to remove side effects in attributes, and
adding support for Stack Map Frames.

Version 8.1.0 includes the following changes:

  * Many compatibility bug fixes.
  * Type inference improvements.
  * Pretty-printing improvements.
  * Many attributes were renamed, removed, added, or redesigned.
  * Stack Map Frames bytecode attributes are now generated by all Java 6+ backends.
  * The Java 6 backend now outputs bytecode version 50 (was 49).
  * The Java 7 backend now outputs bytecode version 51 (was 49).
  * The Java 8 backend now outputs bytecode version 52 (was 49).
  * The AbstractDot node type has been removed.
  * Removed several side effects in attributes. Static semantic error checking
    should now be mostly well-defined with respect to attribute purity and
    circularity.
  * Accessor methods are now generated by NTAs, replacing an imperative AST
    transformation.
  * Removed the FieldDecl list rewrite. Multiple field declarations are now
    handled by using NTAs.
  * Updated to build with JastAdd 2.2.2.
  * Circular NTAs are now used as the rewrite implementation, for safer
    rewrite evaluation.
  * Fixed order dependencies in name classification rewrites.
  * Replaced imperative enclosing variables transformation with on-demand
    generation using NTAs during code generation.
  * Replaced enum index transformations by using NTAs instead.
  * Bridge methods are now generated on demand using NTAs, instead of using
    imperative AST transformations.
  * Replaced implicit enum member rewrites by NTAs.
  * Replaced enum constructor transformation by NTAs.



## 2017-12-09 Stack Map Frames

[Stack map frames](https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-4.html#jvms-4.7.4)
generation was implemented in commit
[21b8c09](https://bitbucket.org/extendj/extendj/commits/21b8c093449a3ae7b2825535bfcff21bc87490be).
This allows ExtendJ to finally generate Java 8 bytecode, which in turn allows
us to compile default interface methods and calls to static interface methods.

## 2017-10-23 Concurrent Circular RAGs Talk

Jesper presented recent work on
Concurrent Circular Reference Attribute Grammars [(doi)](https://doi.org/10.1145/3136014.3136032)
[(free version)](http://portal.research.lu.se/portal/en/publications/concurrent-circular-reference-attribute-grammars-extended-version\(4809a5fc-f7e3-4082-bf41-314091c98e0d\).html)
at SLE 2017. ExtendJ was parallelized as part of this research.
The talk was recorded and can be viewed here:  

<iframe width="560" height="315" src="https://www.youtube.com/embed/TtZZwxTBtRw" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>

## 2017-02-23

Jesper had a talk about ExtendJ at Voxxed Days Z&uuml;rich. The talk is available to
watch on YouTube:

<iframe width="560" height="315" src="https://www.youtube.com/embed/jP76MF59mgo" frameborder="0" allowfullscreen></iframe>

## 2016-10-02 AST Visualizer Tool

A new tool for graphically navigating ExtendJ ASTs is available! Check out the
[new tool page](/visualizer.html) to learn more.

## 2016-02-28 Removing side effects

We are current working on removing side effects in the JastAdd attributes used
in ExtendJ. The goal is to completely remove attribute evaluation order
dependencies and allow interesting parallelization opportunities. To be able
to remove the side effects we have changed the way implicit method declarations,
constructors, and fields are added in the AST.
