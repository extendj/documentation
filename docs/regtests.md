# Regression Test Suite

The ExtendJ regression test suite is [available on Bitbucket][1].  Most of the
tests can be verified against Javac. Only a few test ExtendJ-specific APIs and
features.

To run the tests you need to have a compiled ExtendJ Jar file. It should be
placed in the root directory of the regression test suite and it must be named
`extendj.jar`. After copying the Jar file to the correct location, you
can use Apache Ant to run the tests:

    ant java8


The above line runs the Java 8 tests. To run only the Java 7 tests, use the
command

    ant java7


To run a specific test only, it may be selected using a command-line argument:

    ant -Dtest=type/class_01p


All tests are organized into the `tests` directory, with subdirectories
for different categories of tests. The README has a more thorough description
of how to add new tests.

The test suite contains a mix of regression tests and feature tests for
the Java 7 and Java 8 modules.


[1]: https://bitbucket.org/extendj/regression-tests
