# Integration Testing

Open Source projects being compiled and tested with ExtendJ.

The table below shows the current status of the continuous integration tests.

Project | Java Ver | Version | Test Result | Num Tests | Passing Since | Scm
---|---|---|---|---|---|---
Ant      | 8 | 1.10.1 | **fail** (java8) | | | [GitHub](https://github.com/apache/ant)
Ant      | 5 | 1.9.9  | pass | 2616 | dd7f702253a7fdf74bceae9d5c0dc6314e0aa737 |
Antlr    | 7 | 4.7.1  | pass (no annotproc) | 704 | | [GitHub](https://github.com/antlr/antlr4)
ExtendJ8 | 6 | 8.1.0-27-gcd7effa | pass | 1633 | | [BitBucket](https://bitbucket.org/extendj/extendj)
JaCoP    | 8 | latest | **fail** (java8) | | | [GitHub](https://github.com/radsz/jacop)
JaCoP    | 6 | 4.0.0  | pass (no scala) | 30 | |
JUnit    | 5 | 4.12   | pass | 874  | | [GitHub](https://github.com/junit-team/junit4)
jo-json  | 7 | 1.3.1  | pass | 181  | | [GitHub](https://github.com/llbit/jo-json/blob/master/build.gradle)
jo-nbt   | 8 | 1.1.0  | pass | 37   | | [GitHub](https://github.com/llbit/jo-nbt)

TODO: add more projects.

* AspectJ
* Guava
* ... and more from Qualitas Corpus

## Integration Test Setup

1. Check if annotation processing is needed. ExtendJ does not run annotation processors.
2. Try to compile with the right build system plugin:
    * [plexus-compiler-extendj](https://bitbucket.org/extendj/plexus-compiler-extendj)
    * [gradle-extendj-plugin](https://bitbucket.org/extendj/gradle-extendj-plugin)
    * [ant-extendj-adapter](https://bitbucket.org/extendj/ant-extendj-adapter)
3. Make sure the right ExtendJ release is used, depending on the Java source version.
4. Disable tests that rely on network communication (because they can be flaky).
