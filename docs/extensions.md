#Extensions

This page lists a few projects extending ExtendJ.

Some of these projects use an old version of ExtendJ and are not
compatible with the latest version of ExtendJ.

## Multiplicities

Bitbucket repository: [joqvist/multiplicities][2]

The ideas implemented in the Multiplicities project are described in this research
article:

* Friedrich Steimann, Jesper Öqvist, Görel Hedin: [Multiplicities: First
  implementation and case study][1]

The language extension adds special annotations for variables and fields that
may contain multiple objects. It is sort of like syntactic sugar for
programming with collections:

```
@any Person people;
people += new Person();
people += new Person();
people += new Person();
for (Person person : people) {
  System.out.println(person.getName());
}
```

The compiler extensions included in the Multiplicities module are:

* Scanning and parsing for the special multiplicity operators `[[ ]]` and `|[ ]|`.
* Extended type analysis to handle multiplicity types.
* Code generation to handle updating, comparing, and iterating
  over multitude objects.

Code generation is done by directly generating bytecode, instead of transforming
the syntactic sugar into higher-level constructs.

## NonNull Inference

Bitbucket repository: [jastadd/jastaddj-nonnullinference][3]

The NonNull Inference extension, as the title suggests, adds nonnull inference
in ExtendJ. The extension is no longer actively maintained and not compatible
with recent versions of ExtendJ.  It would take some work to update the extension
to the latest version of ExtendJ, and this has not been done yet.

## SimpleCFG

GitHub repository: [google/simplecfg][4]

The SimpleCFG extension builds control flow graphs on top of the ExtendJ AST. The
main feature of SimpleCFG is that it allows a simple way of building CFGs containing
only nodes of interest. Compared to the intraflow extension, the SimpleCFG graphs
are much more sparse.


## RagDoc Builder

Bitbucket repository: [extendj/ragdoc-builder][6]

A documentation generator used to generate JavaDoc-like documentation for JastAdd projects.
The current ExtendJ documentation is generated using this tool (see link in the sidebar).

RagDoc Builder extends the frontend of ExtendJ and summarizes information about all classes
in a Java project.


## Intraflow

Bitbucket repository: [jastadd/jastaddj-intraflow][5]

This is a control flow graph extension for an old version of ExtendJ. It is no longer
maintained, and not compatible with recent versions of ExtendJ.

[1]: http://dx.doi.org/10.5381/jot.2014.13.5.a1
[2]: https://bitbucket.org/joqvist/multiplicities
[3]: https://bitbucket.org/jastadd/jastaddj-nonnullinference
[4]: https://github.com/google/simplecfg
[5]: https://bitbucket.org/jastadd/jastaddj-intraflow
[6]: https://bitbucket.org/extendj/ragdoc-builder
