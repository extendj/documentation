# ExtendJ - The JastAdd Extensible Java Compiler

ExtendJ, formerly JastAddJ, is an extensible Java compiler built
using the declarative attribute grammar system [JastAdd][1].

Notable features:

* Modular architecutre: extensions can add, remove, and combine features.
* Declarative attributes are used rather than visitors.
* Java support up to Java 11 is mostly complete with some known type inference errors.
* Semantic analysis and bytecode generation.

ExtendJ is useful if you are looking to develop static analyses for Java, or if
you want to prototype new Java language features.


## Getting Started

We have some useful material for new extension developers on the [Getting
Started page][5].

If you just want a quick introduction, please take a look at [these slides][7].

## Development

By fixing bugs and refactoring ExtendJ we risk breaking extension
compatibility. Please keep this in mind if you plan on building an extension.
We recommended that you base your extension on a
specific commit of ExtendJ and upgrade only if you have good test
coverage that can ensure your extension keeps working.

## Java Compatibility

ExtendJ supports Java 5-11 with varying levels of compliance.
The Java 8 support is close to feature complete with some known type inference problems.
Java 9 through 11 support is available but not thoroughly tested.


We work toward improving Java compatibility by finding and fixing bugs. This
requires a lot of testing, and we are very thankful for any compatibility issues
that you report.  Please look at the [compiance issues page][6] for more
information.  If you encounter a Java compliance error, please report it to
[our issue tracker][4].

To test compatibility we use continuous integration to compile and test Open
Source projects with ExtendJ. Currently each commit of ExtendJ is tested on Ant
1.9.9, Antlr 4.7.1, JaCoP 4.0.0, and JUnit 4.12.

## Development History

ExtendJ was originally created by Torbj&ouml;rn Ekman at Lund University.
Improvements have been made by several contributors over the years:

* Torbj&ouml;rn Ekman developed the original compiler.
* Emma S&ouml;derberg worked on refactoring type lookup to make it more declarative.
* Jesper &Ouml;qvist worked on the Java 7 extension, stack map frames generation,
  and many bug fixes / refactorings since then.
* Erik Hogeman implemented Java 8 support in 2014.
* David Björk and Johannes Aronsson implemented Java 9, 10, and 11 support in 2023.

ExtendJ is currently maintained by Jesper &Ouml;qvist. Issues can be reported
on the [ExtendJ Issue Tracker](https://bitbucket.org/extendj/extendj/issues).

ExtendJ is provided as Open Source under the [Modified BSD License][3].
Links to our Git repositories can be found on the [source code page][3].

The latest ExtendJ release is @VERSION@.

[1]: http://jastadd.org
[2]: https://bitbucket.org/extendj/extendj/wiki/splash2015tutorial
[3]: /source_code.html
[4]: https://bitbucket.org/extendj/extendj/issues?status=new&status=open
[5]: /getting_started.html
[6]: /compliance.html
[7]: https://docs.google.com/presentation/d/1entbtOvW1xJpMvQl1HxLAI4KFCelemuKQqGDND1jJVs/edit#slide=id.p
