# NeoBeaver

[NeoBeaver](https://bitbucket.org/joqvist/neobeaver) is a re-implementation of
the Beaver LALR parser generator.

NeoBeaver extends the syntax for Beaver parser specifications to make it easier to write new
grammars. Notable improvements are:

* Improved warning & error messages.
* Inline type declarations (`%typeof` is optional).
* Implicit terminals (`%terminals` is optional).
* Automatic component names.
* Unused terminals are not removed (makes it easier to bootstrap a new scanner).

NeoBeaver aims to be fully forward-compatible for Beaver grammar
specifications.

The goal for ExtendJ is to eventually use NeoBeaver as a replacement for
Beaver, but the tool is not mature enough yet.
