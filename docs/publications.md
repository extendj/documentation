#Publications

Note: ExtendJ was formerly known as JastAddJ, and most past publications use
the old name. In some cases ExtendJ is also referred to as the JastAdd frontend
for Java.

This OOPSLA 2007 paper describes the basic architecture of ExtendJ:

* Torbj&ouml;rn Ekman, G&ouml;rel Hedin: [The JastAdd Extensible Java Compiler](http://fileadmin.cs.lth.se/sde/publications/papers/2007-Ekman-OOPSLA.html). OOPSLA 2007: 1-18, Proceedings of the 22nd Annual ACM SIGPLAN Conference on Object-Oriented Programming, Systems, Languages, and Applications, October 2007, Montreal, Canada.

The development of the  Java 7 extension is described in a Master's Thesis and
a later research paper:

* Jesper &Ouml;qvist: [Implementation of Java 7 Features in an Extensible Compiler](http://sam.cs.lth.se/ExjobGetFile?id=477). Master's Thesis LU-CS-EX: 2012-13, Dept of Computer Science, Lund University, 2012.
* Jesper &Ouml;qvist, G&ouml;rel Hedin: [Extending the JastAdd extensible Java compiler to Java 7](http://dx.doi.org/10.1145/2500828.2500843). PPPJ 2013:147-152, Proceedings of the 2013 International Conference on Principles and Practices of Programming on the Java Platform: Virtual Machines, Languages, and Tools, September 2013, Stuttgart, Germany.

The development of the Java 8 module is documented in this Master's Thesis:

* Erik Hogeman: [Extending JastAddJ to Java 8](http://sam.cs.lth.se/ExjobGetFile?id=477). Master's Thesis LU-CS-EX: 2014-14, Dept of Computer Science, Lund University, 2014.

## Extension Articles

The following article describes an implementation of Multiplicities for Java,
as an ExtendJ Extension:

* Friedrich Steimann, Jesper &Ouml;qvist, G&ouml;rel Hedin: [Multiplicities: First
  implementation and case study][1]


## Research Using ExtendJ

The following research papers mention using ExtendJ (JastAddJ) to build analyses or
language features on top of Java:

* Jan C. Dagef&ouml;rde, Herbert Kuchen:[A Constraint-Logic Object-Oriented Language][12] (SAC, 2018)
* Chong Shum, Wing-Hong Lau, Tian Mao, Henry Shu-Hung Chung, Kim-Fung Tsang,
Norman Chung-Fai Tse, Loi Lei Lai: [Co-Simulation of Distributed Smart Grid Software
Using Direct-Execution Simulation][11] (IEEE Access, 2018)
* Chong Shum, Wing-Hong Lau, Tian Mao, Henry Shu-Hung Chung, Norman Chung-Fai Tse, Kim-Fung Tsang,
Loi Lei Lai: [DecompositionJ: Parallel and Deterministic Simulation of Concurrent Java Executions in Cyber-physical Systems][10] (IEEE Access, 2018)
* Tetsuo Kamina, Tomoyuki Aotani: [Harmonizing Signals and Events with a Lightweight Extension to
Java][9] (The Art, Science, and Engineering of Programming, 2018)
* Mohammad Reza Azadmanesh, Matthias Hauswirth: [Concept-Driven Generation of Intuitive Explanations of Program Execution for a Visual Tutor.][5] (Software Visualization, 2017)
* Sukyoung Ryu: [ThisType for Object-Oriented Languages: From Theory to Practice.][2] (ACM Transactions on Programming Languages and Systems, 2016)
* YungYu Zhuang, Shigeru Chiba: [Expanding Event Systems to Support Signals by Enabling the Automation of Handler Bindings.][3] (Journal of Information Processing, 2016)
* Friedrich Steimann, J&ouml;rg Hagemann, Bastian Ulke: [Computing repair alternatives for malformed programs using constraint attribute grammars.][4] (OOPSLA, 2016)
* Vivek Kumar, Julian Dolby, Stephen M. Blackburn: [Integrating Asynchronous Task Parallelism and Data-centric Atomicity.][6] (PPPJ, 2016)
* Yusheng Weijiang, Shruthi Balakrishna, Jianqiao Liu, Milind Kulkarni: [Tree dependence analysis.][8] (PLDI, 2015)
* Eric Bodden, &Eacute;ric Tanter, Milton Inostroza: [Join point interfaces for safe and flexible decoupling of aspects.][7] (ACM Transactions on Software Engineering and Methodology, 2014)


[1]: http://dx.doi.org/10.5381/jot.2014.13.5.a1
[2]: http://doi.acm.org/10.1145/2888392
[3]: https://doi.org/10.2197/ipsjjip.24.620
[4]: http://doi.acm.org/10.1145/2983990.2984007
[5]: https://doi.org/10.1109/VISSOFT.2017.22
[6]: http://doi.acm.org/10.1145/2972206.2972214
[7]: http://doi.acm.org/10.1145/2559933
[8]: http://doi.acm.org/10.1145/2737924.2737972
[9]: https://arxiv.org/pdf/1803.10199.pdf
[10]: https://doi.org/10.1109/ACCESS.2018.2825254
[11]: https://doi.org/10.1109/ACCESS.2018.2824341
[12]: https://doi.org/10.1145/3167132.3167260
