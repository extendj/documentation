# ExtendJ Site

Website for ExtendJ. All pages are written in [Markdown][3]. Static HTML files
are generated from the Markdown sources.

## Dependencies

Python 3 is required to generate the documentation pages.

**Optional:** http-server can be used to view the generated documentation.
Install http-server with npm (Node Package Manager):

    npm install http-server -g


Alternatively, use a [small nodejs web server script][2].


## Testing

The webpages can be generated in the local directory `out` using the
[gradlew][1] script in the project root.

To generate and view the webpages:

    ./gradlew
    http-server out -o --cors



## Template Expansions

The build script runs a Python script named `tools/mdwrapper.py` on all
Markdown files (`*.md`) in the `docs` directory. Before the Python script is
run, some special tokens such as `%VERSION%` are replaced with the values listed
in the `version.properties` file. The build script then creates thumbnails for
the gallery and finally copies all files in the `images` and `style`
directories into the output directory.


[1]:http://gradle.org/
[2]:http://stackoverflow.com/a/13635318
[3]:http://daringfireball.net/projects/markdown/syntax
